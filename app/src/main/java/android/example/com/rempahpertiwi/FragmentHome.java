package android.example.com.rempahpertiwi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class FragmentHome extends Fragment {

    View v;
    private RecyclerView myreceyclerview;
    private List<Product> lstProduct;


    public FragmentHome(){
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, container, false);
        myreceyclerview = (RecyclerView) v.findViewById(R.id.product_recyclerview);
        RecyclerViewAdapter recyclerAdapter = new RecyclerViewAdapter(getContext(), lstProduct);
        myreceyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        myreceyclerview.setAdapter(recyclerAdapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstProduct = new ArrayList<>();
        lstProduct.add(new Product("Bawang Putih", "Rp.35.000", R.drawable.bawang_putih));
        lstProduct.add(new Product("Bawang Merah", "Rp.35.000", R.drawable.bawang_merah));
        lstProduct.add(new Product("Cabai", "Rp.35.000", R.drawable.cabai));
        lstProduct.add(new Product("Kunyit", "Rp.35.000", R.drawable.kunyit));
        lstProduct.add(new Product("Kemiri", "Rp.35.000", R.drawable.kemiri));
        lstProduct.add(new Product("Kayu Manis", "Rp.35.000", R.drawable.kayu_manis));
        lstProduct.add(new Product("Adas Bintang", "Rp.35.000", R.drawable.adas_bintang));
        lstProduct.add(new Product("Kayu Manis", "Rp.35.000", R.drawable.kayu_manis));

    }
}
