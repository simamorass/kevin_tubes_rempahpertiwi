package android.example.com.rempahpertiwi;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class DB_Regitrasi implements Serializable {
    private String nama;
    private String email;
    private String password;
    private String key;

    public DB_Regitrasi(){

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return " "+nama+"\n" +
                " "+email+"\n" +
                " "+password;
    }

    public DB_Regitrasi(String nama, String email, String password){
        this.nama = nama;
        this.email = email;
        this.password = password;
    }
}