package android.example.com.rempahpertiwi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
///////////////////////
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
///////////////////////

public class activity_daftar extends AppCompatActivity {

    // variable yang merefers ke Firebase Realtime Database
    private DatabaseReference database;

    private Button btnDaftar;
    TextView txtLogin;

    private EditText eNale, eEmail, ePassword;
    private FirebaseAuth auth;

    //////////////////////////////////////////////////////////
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);



        initView();

        // mengambil referensi ke Firebase Database
        database = FirebaseDatabase.getInstance().getReference();

        // kode yang dipanggil ketika tombol Submit diklik
        /*btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isEmpty(eNale.getText().toString()) && !isEmpty(eEmail.getText().toString()) && !isEmpty(ePassword.getText().toString()))
                    sumbitReg(new DB_Regitrasi(eNale.getText().toString(), eEmail.getText().toString(), ePassword.getText().toString()));
                else
                    Snackbar.make(findViewById(R.id.btnDaftar), "Data barang tidak boleh kosong", Snackbar.LENGTH_LONG).show();

                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        eNale.getWindowToken(), 0);
            }
        });*/

        registerUser();
    }


    private boolean isEmpty(String s) {
        // Cek apakah ada fields yang kosong, sebelum disubmit
        return TextUtils.isEmpty(s);
    }

    private void sumbitReg(DB_Regitrasi regis) {
        /**
         * Ini adalah kode yang digunakan untuk mengirimkan data ke Firebase Realtime Database
         * dan juga kita set onSuccessListener yang berisi kode yang akan dijalankan
         * ketika data berhasil ditambahkan
         */
        database.child("registrasi").push().setValue(regis).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                eNale.setText("");
                eEmail.setText("");
                ePassword.setText("");
                Snackbar.make(findViewById(R.id.btnDaftar), "Data berhasil ditambahkan", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public static Intent getActIntent(Activity activity) {
        // kode untuk pengambilan Intent
        return new Intent(activity, activity_daftar.class);
    }

    private void registerUser() {
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //menampung inputan user
                String emailUser = eEmail.getText().toString().trim();
                String passwordUser = ePassword.getText().toString().trim();
                //String namaLengkap = eNale.getText().toString().trim();

                //validasi nama, email dan password
                // jika email kosong
                if (emailUser.isEmpty()){
                    eEmail.setError("Email tidak boleh kosong");
                }
                // jika email not valid
                else if (!Patterns.EMAIL_ADDRESS.matcher(emailUser).matches()){
                    eEmail.setError("Email tidak valid");
                }
                // jika password kosong
                else if (passwordUser.isEmpty()){
                    ePassword.setError("Password tidak boleh kosong");
                }
                //jika password kurang dari 6 karakter
                else if (passwordUser.length() < 6){
                    ePassword.setError("Password minimal terdiri dari 6 karakter");
                }
                else {
                    //create user dengan firebase auth
                    auth.createUserWithEmailAndPassword(emailUser,passwordUser)
                            .addOnCompleteListener(activity_daftar.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    //jika gagal register do something
                                    if (!task.isSuccessful()){
                                        Toast.makeText(activity_daftar.this,
                                                "Register gagal karena "+ task.getException().getMessage(),
                                                Toast.LENGTH_LONG).show();
                                    }else {
                                        //jika sukses akan menuju ke login activity
                                        if(!isEmpty(eNale.getText().toString()) && !isEmpty(eEmail.getText().toString()) && !isEmpty(ePassword.getText().toString()))
                                            sumbitReg(new DB_Regitrasi(eNale.getText().toString(), eEmail.getText().toString(), ePassword.getText().toString()));
                                        else
                                            Snackbar.make(findViewById(R.id.btnDaftar), "Data barang tidak boleh kosong", Snackbar.LENGTH_LONG).show();

                                        InputMethodManager imm = (InputMethodManager)
                                                getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(
                                                eNale.getWindowToken(), 0);
                                        startActivity(new Intent(activity_daftar.this,activity_login.class));

                                    }
                                }
                            });
                }
            }
        });
    }

    private void initView() {
        eNale = (EditText) findViewById(R.id.eNale);
        eEmail = (EditText) findViewById(R.id.eEmail);
        ePassword = (EditText) findViewById(R.id.ePassword);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        auth = FirebaseAuth.getInstance();
    }

}