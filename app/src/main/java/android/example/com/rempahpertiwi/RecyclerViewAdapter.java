package android.example.com.rempahpertiwi;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    Context mContext;
    List<Product> mData;
    Dialog myDialog;

    public RecyclerViewAdapter(Context mContext, List<Product> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v ;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_product, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(v);

        myDialog = new Dialog(mContext);
        myDialog.setContentView(R.layout.details_product);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        viewHolder.item_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView detail_name_tv = (TextView) myDialog.findViewById(R.id.details_name_id);
                TextView detail_harga_tv = (TextView) myDialog.findViewById(R.id.details_harga_id);
                ImageView detail_img_tv = (ImageView) myDialog.findViewById(R.id.details_img_id);
                detail_name_tv.setText(mData.get(viewHolder.getAdapterPosition()).getName());
                detail_harga_tv.setText(mData.get(viewHolder.getAdapterPosition()).getPrice());
                detail_img_tv.setImageResource(mData.get(viewHolder.getAdapterPosition()).getImage());
                Toast.makeText(mContext, "On Click"+String.valueOf(viewHolder.getAdapterPosition()), Toast.LENGTH_SHORT).show();
                myDialog.show();
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, int position) {

        holder.tv_name.setText(mData.get(position).getName());
        holder.tv_price.setText(mData.get(position).getPrice());
        holder.img.setImageResource(mData.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout item_product;
        private TextView tv_name;
        private TextView tv_price;
        private ImageView img;


        public MyViewHolder(View itemView) {
            super(itemView);

            item_product = (LinearLayout) itemView.findViewById(R.id.product_item);
            tv_name = (TextView) itemView.findViewById(R.id.name);
            tv_price = (TextView) itemView.findViewById(R.id.price);
            img = (ImageView) itemView.findViewById(R.id.image);

        }
    }

}
